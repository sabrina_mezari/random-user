import React,{ useState, useContext } from "react";
import { AxiosContext } from "../context/axiosContext";

import UserForm from "./UserForm";
import "../style/tailwind.css";

import {
  RiMailLine,
  RiCalendarEventFill,
  RiPhoneFill,
  RiEditLine
} from "react-icons/ri";


function UserCard() {

    const {user, setUser, isUsersDataLoading} = useContext(AxiosContext);

    const [userEditFormData, setUserEditFormData] = useState(null);

    function initializeUserEditForm() {
        //should use a proper deep copy lib if performance needed
        const deepCopyUser = JSON.parse(JSON.stringify(user))
        setUserEditFormData(deepCopyUser)
    }

    function onChangeEmailData(e) {
        const userUpdateData = {
            ...userEditFormData,
            email: e.target.value,
          }
          setUserEditFormData(userUpdateData);
    }

    function onChangePhoneData(e) {
      const userUpdateData = {
        ...userEditFormData,
        phone: e.target.value,
      }
      setUserEditFormData(userUpdateData);
  
    }

    function updateDataOnSubmit() {    
        setUser(userEditFormData);
        setUserEditFormData(null);
    }
   

    return(
        <div>
            <div className="text-slate-50">

                {/* when data are loading */}
                {isUsersDataLoading  || !user ? (

                <p className="text-4xl text-center m-10">loading...</p>

                ) : (
                    <div>                           
                        <div>
                            {/* title */}
                            <h1 className=" text-slate-50 text-4xl text-center m-4 mb-20 mt-10">Hello Lunatech ! I'm {user.name.first}</h1>  

                            {/* user card container */}
                            <div className="w-full max-w-sm bg-slate-200 rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700 block m-auto">
                                            
                                {/* logo to edit data */}
                                <div className="flex justify-end px-4 pt-4">  
                                    <RiEditLine 
                                        className="mb-1 mr-4 text-xl font-medium text-gray-900 dark:text-white hover:text-orange-500 cursor-pointer" 
                                        onClick={initializeUserEditForm}
                                    />
                                </div>

                                {/* user data */}
                                <div className="block m-auto">
                                    {/* img profile */}
                                    <div>
                                        <img className="mb-3 w-24 h-24 rounded-full shadow-lg block m-auto" src={user.picture.medium} alt="img-profile"/>
                                    </div>

                                    {/* name data */}
                                    <div>
                                        <h3 className="mb-10 mt-10 text-xl font-medium text-gray-900 dark:text-white text-center">{user.name.first} {user.name.last}</h3>
                                    </div>

                                    {/* email data */}
                                    <div className="flex items-center ml-20">
                                        <RiMailLine className="mb-1 text-sm mr-4 text-gray-500 dark:text-gray-300" />
                                        <p className="mb-1 text-sm text-gray-500 dark:text-gray-300">{user.email}</p>
                                    </div>

                                    {/* dob data */}
                                    <div className="flex items-center ml-20">
                                        <RiCalendarEventFill className="mb-1 text-sm mr-4 text-gray-500 dark:text-gray-300"/>
                                        <p className="mb-1 text-sm text-gray-500 dark:text-gray-300">{user.dob.age} ans</p>
                                    </div>

                                    {/* phone data */}
                                    <div className="flex items-center ml-20 mb-20">
                                        <RiPhoneFill className="mb-1 text-sm mr-4 text-gray-500 dark:text-gray-300"/>
                                        <p className="mb-1 text-sm text-gray-500 dark:text-gray-300 ">{user.phone}</p>
                                    </div>

                                </div> 
                            </div> 
                            {/* user card end */}

                            
                            {/* user form to update data */}
                            <UserForm 
                                userEditFormData = {userEditFormData}
                                onChangeEmailData = {onChangeEmailData}
                                onChangePhoneData = {onChangePhoneData}
                                updateDataOnSubmit = {updateDataOnSubmit}
                            />
                                    
                        </div>
                                      
                    </div>
                    )
                }

            </div>
        </div>
    )
}

export default UserCard;
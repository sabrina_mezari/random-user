function UserForm({userEditFormData, onChangeEmailData, onChangePhoneData, updateDataOnSubmit}) {
    return (
        <div>

            {userEditFormData && 

                <form className="w-full max-w-sm block m-auto">
                    <p className="text-center mb-4 mt-6"> You can update your data :</p>
                                    
                    <div className="flex mb-1">
                        <label 
                            className="block text-gray-500 font-bold m-1 pr-4" 
                            id="inline-full-name">
                        Email 
                        </label>
                                        
                        <input 
                            className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 ml-3 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-orange-400" 
                            placeholder="Enter your new phone"
                            id="inline-full-name"
                            type="text"
                            value={userEditFormData && userEditFormData.email}
                            onChange={(e) => onChangeEmailData(e)}
                        />
                    </div>
                                    
                    <div className="flex mb-2">
                        <label 
                            className="block text-gray-500 font-bold m-1 pr-4" 
                            id="inline-full-name">
                        Phone
                        </label>

                        <input 
                            className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 ml-1 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-orange-400"
                            placeholder="Enter your new phone"
                            id="inline-full-name"
                            type="text"
                            value={userEditFormData && userEditFormData.phone}
                            onChange={(e) => onChangePhoneData(e)}
                        />
                    </div>

                    <button 
                        className="shadow bg-slate-50 hover:bg-slate-200 focus:shadow-outline focus:outline-none text-slate-400 font-bold py-2 px-4 rounded block m-auto"
                        onClick={updateDataOnSubmit}>
                    Update
                    </button>
                </form>  
            }
        </div>
    )
}

export default UserForm;
function Navigation() {
    return (
        <div className="m-7">
            <ul >
                <li> 
                    <a className="text-slate-50 hover:text-green-200 absolute top-0 right-0 p-7" href="/">Home</a>
                </li>
                
            </ul>
        </div>
    )
}

export default Navigation;
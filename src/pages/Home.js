import { useNavigate } from "react-router-dom";


function Home() {

    let navigate = useNavigate();

    function redirectToUserPageOnClick() {
    navigate("/User")
  }

    return (
        <div>
           <h1 className="text-5xl font-medium text-center mt-40">Click on the button to generate a random user</h1>
            <button 
                className="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow content-center 
                absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2"
                onClick={redirectToUserPageOnClick}
            >The button </button>   
        </div>
    )
}

export default Home;
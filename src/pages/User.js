import Navigation from "../components/navigation";
import UserCard from "../components/UserCard";

function User() {
    return (
        <div>
            <div>
                <Navigation />
                <UserCard />
            </div>
        </div>
    )
}

export default User;
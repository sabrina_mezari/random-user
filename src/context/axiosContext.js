import React, { createContext, useState, useEffect } from "react";
import axios from "axios";

export const AxiosContext = createContext();

export const AxiosContextProvider = ({children}) => {

    const [user, setUser] = useState(null);
    const [isUsersDataLoading, setIsUsersDataLoading] = useState(false);


    useEffect(() => {  

        const getUsersData = async() => {

          setIsUsersDataLoading(true);

          const response = await axios.get("https://randomuser.me/api")
          setUser(response.data.results[0]);
    
          setIsUsersDataLoading(false);
        };
        getUsersData()
      }, []);


    return (
        <AxiosContext.Provider value={{user, setUser, isUsersDataLoading, setIsUsersDataLoading}}>
            {children}
        </AxiosContext.Provider>
    )
}
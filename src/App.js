import { 
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import { AxiosContextProvider } from "./context/axiosContext";
import "./style/tailwind.css";


import Home from "./pages/Home"
import User from "./pages/User"


function App() {

  return (
    <>
      <AxiosContextProvider>
          <BrowserRouter>
                  <Routes>
                      <Route path="/" index element={<Home />}/>
                      <Route path="/user" element={<User />}/>
                  </Routes>
              </BrowserRouter>
      </AxiosContextProvider>
    </>  
  );
}

export default App;

